# GprinterSDKSample

參考林助銓<dean4101@gmail.com>寫的iOS SDK製作的Gprinter的Android library+Sample。

## Requirements

Android 4.2 (API 17) 以上

## About Sample Project

#### Demo功能
- 切換USB/無線連線方式
- 列印測試頁
- 列印單張圖片
- 簡單的走紙4行+切紙
- 開錢箱
- 即時讀取 印表機狀態/脫機狀態/錯誤狀態/傳送紙狀態
- 圖片二值化功能 (使用OTSU演算法計算閥值)
- 測試列印電子發票+銷售明細 (假資料)

## About RTGprinterHelper

用來簡單控制Gprinter的Android library<br>
因為普通的印文字無法完全適用於每台Gprinter，因此library內主要是以印圖片`Bitmap`的方式為主

### Classes
- `RTGprinterCommand`: 用於產生輸出給Gprinter的指令
- `RTGprinterHelper`: 用於與Gprinter連線、發送及接收資料用
- `RTGprinterUsbManager`: 用於取得所有連接到裝置的Gprinter的`UsbDevice`實體

## Author

Chih-chieh Chang, ccch.realtouch@gmail.com

## License

GprinterSDKSample is available under the MIT license. See the LICENSE file for more info.
