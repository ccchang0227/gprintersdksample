package com.realtouchapp.gprintersdksample;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoadingDialog {
	
	
	private String text="";
	private Dialog dialog = null;
	private Context context;
	private TextView tv_hint;
	private StopLoadingListener stopLoadingListener =null;
	public LoadingDialog(Context context,String text) {			
		this.context=context;
		this.text=text;
		findViews();
		setDialog();
		setView();
		
	}
	
	public LoadingDialog(Context context,String text,StopLoadingListener stopLoadingListener) {			
		this.context=context;
		this.text=text;
		this.stopLoadingListener=stopLoadingListener;		
		findViews();
		setDialog();
		setView();
		
	}
	public LoadingDialog(Context context) {			
		this.context=context;
		this.text="";
		findViews();
		setDialog();
		setView();
		
	}
	public boolean isShowing() {
		return dialog.isShowing();
	}
	public void show(){
		
		dialog.show();
	}
	private void findViews() {
		dialog = new Dialog(context,R.style.dialog_customize_full_screen);
//		dialog=new Dialog(context ,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
		dialog.setContentView(R.layout.dialog_main_search);
	
	}
	private void setDialog(){		   	    	    	  
	    Window dialogWindow = dialog.getWindow();
		dialogWindow.setGravity(Gravity.CENTER_HORIZONTAL |Gravity.TOP);
		dialogWindow.setTitle(null);
		//設定軟件盤彈出時 會適應大小
		dialogWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		//關閉Back建
    	dialog.setCancelable(false);
    	dialog.setCanceledOnTouchOutside(false);
	}
	
	private void setView(){
		int width=UserInterfaceTool.getScreenWidthPixels(context);
		int heigh=UserInterfaceTool.getScreenHeightPixels(context);
		RelativeLayout rl_loading=(RelativeLayout)dialog.findViewById(R.id.rl_loading);
		UserInterfaceTool.setViewSize(rl_loading, width, heigh);
		ProgressBar pb_loading=(ProgressBar)dialog.findViewById(R.id.pb_loading);
		UserInterfaceTool.setViewSize(pb_loading, heigh/4, heigh/4);
		tv_hint=(TextView)dialog.findViewById(R.id.tv_hint);
		UserInterfaceTool.setTextSize(tv_hint, 10);
		if (text==null|text.length()==0) {
			tv_hint.setText("");
		}else{
			tv_hint.setText(text);	
		}
		
		if (stopLoadingListener != null) {
			Button btn_loading_cancle=(Button) dialog.findViewById(R.id.btn_loading_cancle);
			btn_loading_cancle.setVisibility(View.VISIBLE);			
			btn_loading_cancle.setOnClickListener(new View.OnClickListener() {				
				@Override
				public void onClick(View v) {
					stopLoadingListener.onCancle();
					
				}
			});
		}
		
		
	}
	/**動態更新文字使用*/
	public void setText(String text){
		tv_hint.setText(text);
	}
	public void dismiss(){
		if (dialog.isShowing()) {
			dialog.dismiss();	
		}		
	}

	public Context getContext(){
		return context;
	}
	
	public interface StopLoadingListener{
		abstract void onCancle();
	}
}
