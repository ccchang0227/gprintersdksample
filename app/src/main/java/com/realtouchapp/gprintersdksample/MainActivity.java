package com.realtouchapp.gprintersdksample;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.usb.UsbConfiguration;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.datamatrix.encoder.SymbolShapeHint;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.realtouchapp.PrinterBase;
import com.realtouchapp.barcodeencoder.MyCode39Writer;
import com.realtouchapp.barcodeencoder.MyQRCodeWriter;
import com.realtouchapp.dpembeddedprinter.DPEmbeddedPrinterCommand;
import com.realtouchapp.dpembeddedprinter.DPEmbeddedPrinterHelper;
import com.realtouchapp.dpembeddedprinter.DPEmbeddedPrinterUsbManager;
import com.realtouchapp.gprinter.GprinterCommand;
import com.realtouchapp.gprinter.GprinterHelper;
import com.realtouchapp.gprinter.GprinterUsbManager;

import java.io.UnsupportedEncodingException;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("unused")
public class MainActivity extends Activity {
    private final static String tag = "MainActivity";

    private CheckBox cb_use_embedded_printer;
    private CheckBox cb_use_usb;
    private EditText et_host;
    private EditText et_port;

    private TextView tv_debug;

    private boolean printImage = false;

    private LoadingDialog loadingDialog;

    private String[] mipmapTitles = {
                                    "銜尾蛇"
                                    ,"人體鍊成陣"
                                    ,"國土鍊成陣"
                                    ,"Lena"
                                    ,"可愛的小妹妹"
                                    ,"老闆"
                                    ,"吉芳"
                                    ,"信廷1"
                                    ,"信廷2"
                                    ,"柏翰"
                                    ,"韋舜"
                                    ,"致融"
                                    ,"大誠"
                                    ,"嘉桓"
                                    ,"信杰"
                                    ,"凱堯"
                                    ,"脩得"
                                    ,"助銓"
                                    ,"智傑"
                                    ,"國銘"
                                    ,"孝先"
                                    ,"旻坤"
                                    };
    private int[] mipmapIDs = {
                                R.mipmap.test,
                                R.mipmap.test2,
                                R.mipmap.test3,
                                R.mipmap.lena,
                                R.mipmap.loli,
                                R.mipmap.boss,
                                R.mipmap.fang,
                                R.mipmap.shinting,
                                R.mipmap.shinting2,
                                R.mipmap.pohan,
                                R.mipmap.weishun,
                                R.mipmap.ron,
                                R.mipmap.bigchen,
                                R.mipmap.huang,
                                R.mipmap.shinjie,
                                R.mipmap.kaiyau,
                                R.mipmap.lauda,
                                R.mipmap.cclin,
                                R.mipmap.jie,
                                R.mipmap.ismyaki,
                                R.mipmap.hhlin,
                                R.mipmap.robertchou
                                };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cb_use_embedded_printer = (CheckBox) findViewById(R.id.cb_use_embedded_printer);
        cb_use_embedded_printer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                cb_use_usb.setEnabled(!isChecked);
                if (isChecked) {
                    et_host.setEnabled(false);
                    et_port.setEnabled(false);
                }
                else {
                    et_host.setEnabled(!cb_use_usb.isChecked());
                    et_port.setEnabled(!cb_use_usb.isChecked());
                }
            }
        });

        cb_use_usb = (CheckBox) findViewById(R.id.cb_use_usb);
        cb_use_usb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                et_host.setEnabled(!isChecked);
                et_port.setEnabled(!isChecked);
            }
        });

        et_host = (EditText) findViewById(R.id.et_host);
        et_port = (EditText) findViewById(R.id.et_port);

        tv_debug = (TextView) findViewById(R.id.tv_debug);

        // 列印測試頁
        View btn_self_test = findViewById(R.id.btn_self_test);
        btn_self_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != loadingDialog && loadingDialog.isShowing()) {
                    return;
                }

                loadingDialog = new LoadingDialog(MainActivity.this);
                loadingDialog.show();

                getPrinter().printTestPage(getCallback());

            }
        });

        // 列印圖片
        View btn_print_image = findViewById(R.id.btn_print_image);
        registerForContextMenu(btn_print_image);
        btn_print_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printImage = true;
                openContextMenu(v);
            }
        });

        // 圖片二值化
        View btn_process_image = findViewById(R.id.btn_process_image);
        registerForContextMenu(btn_process_image);
        btn_process_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printImage = false;
                openContextMenu(v);
            }
        });

        // 走紙5行+切紙
        View btn_print_line_and_cut = findViewById(R.id.btn_print_line_and_cut);
        btn_print_line_and_cut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != loadingDialog && loadingDialog.isShowing()) {
                    return;
                }

                loadingDialog = new LoadingDialog(MainActivity.this);
                loadingDialog.show();

                byte[] cmd;
                if (cb_use_embedded_printer.isChecked()) {
                    cmd = new DPEmbeddedPrinterCommand.Builder()
                            .append(DPEmbeddedPrinterCommand.printAndFeedLines(5))
                            .append(DPEmbeddedPrinterCommand.cutPaper())
                            .create();
                }
                else {
                    cmd = new GprinterCommand.Builder()
                            .append(GprinterCommand.printAndFeedLines(5))
                            .append(GprinterCommand.cutPaper())
                            .create();
                }

                getPrinter().sendData(cmd, getCallback());
            }
        });

        // 開錢箱
        View btn_open_cash_drawer = findViewById(R.id.btn_open_cash_drawer);
        btn_open_cash_drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != loadingDialog && loadingDialog.isShowing()) {
                    return;
                }

                loadingDialog = new LoadingDialog(MainActivity.this);
                loadingDialog.show();

                byte[] openCashDrawer;
                if (cb_use_embedded_printer.isChecked()) {
                    openCashDrawer = DPEmbeddedPrinterCommand.openCashDrawer();
                }
                else {
                    openCashDrawer = GprinterCommand.openCashDrawer();
                }

                getPrinter().sendData(openCashDrawer, getCallback());
            }
        });

        // 列印發票+銷貨明細
        View btn_print_invoice = findViewById(R.id.btn_print_invoice);
        btn_print_invoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // test print fake invoice
                printInvoiceAndDetail();
            }
        });

        // 列印純文字
        View btn_print_plain_text = findViewById(R.id.btn_print_plain_text);
        btn_print_plain_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != loadingDialog && loadingDialog.isShowing()) {
                    return;
                }

                byte[] command_header = {0x1c, 0x26, 0x1b, 0x39, 0x00};
                String text = "Hello World!\n列印純文字";
                byte[] cmd;
                try {
                    if (cb_use_embedded_printer.isChecked()) {
                        cmd = new DPEmbeddedPrinterCommand.Builder()
                                .append(command_header)
                                .append(text.getBytes("GBK"))
                                .append(DPEmbeddedPrinterCommand.printAndFeedLines(4))
                                .append(DPEmbeddedPrinterCommand.cutPaper())
                                .create();
                    }
                    else {
                        cmd = new GprinterCommand.Builder()
                                .append(command_header)
                                .append(text.getBytes("GBK"))
                                .append(GprinterCommand.printAndFeedLines(4))
                                .append(GprinterCommand.cutPaper())
                                .create();
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                    return;
                }

                loadingDialog = new LoadingDialog(MainActivity.this);
                loadingDialog.show();

                getPrinter().sendData(cmd, getCallback());

            }
        });

        // 打印机状态
        View btn_printer_status = findViewById(R.id.btn_printer_status);
        btn_printer_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != loadingDialog && loadingDialog.isShowing()) {
                    return;
                }

                loadingDialog = new LoadingDialog(MainActivity.this);
                loadingDialog.show();

                byte[] command = {0x10, 0x04, 0x01};

                getPrinter().sendSingleData(command, getCallback2((TextView) findViewById(R.id.tv_printer_status)));
            }
        });

        // 脱机状态
        View btn_connect_status = findViewById(R.id.btn_connect_status);
        btn_connect_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != loadingDialog && loadingDialog.isShowing()) {
                    return;
                }

                loadingDialog = new LoadingDialog(MainActivity.this);
                loadingDialog.show();

                byte[] command = {0x10, 0x04, 0x02};

                getPrinter().sendSingleData(command, getCallback2((TextView) findViewById(R.id.tv_connect_status)));

                /* 測試印文字+修改文字大小
                byte[] cmd;
                try {
                    String str = "ABc\n";
                    cmd = new GprinterCommand.Builder()
                                                .append(new byte[]{0x1d, 0x21, 0x00})
                                                .append(str.getBytes("GB2312"))
                                                .append(new byte[]{0x1d, 0x21, 0x11})
                                                .append(str.getBytes("GB2312"))
                                                .append(new byte[]{0x1d, 0x21, 0x22})
                                                .append(str.getBytes("GB2312"))
                                                .append(new byte[]{0x1d, 0x21, 0x33})
                                                .append(str.getBytes("GB2312"))
                                                .append(new byte[]{0x1d, 0x21, 0x44})
                                                .append(str.getBytes("GB2312"))
                                                .append(new byte[]{0x1d, 0x21, 0x55})
                                                .append(str.getBytes("GB2312"))
                                                .append(new byte[]{0x1d, 0x21, 0x66})
                                                .append(str.getBytes("GB2312"))
                                                .append(new byte[]{0x1d, 0x21, 0x77})
                                                .append(str.getBytes("GB2312"))
                                                .append(GprinterCommand.printAndFeedLines(4))
                                                .append(GprinterCommand.cutPaper())
                                                .create();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                    return;
                }

                getPrinter().sendData(cmd, getCallback());
                */

            }
        });

        // 错误状态
        View btn_error_status = findViewById(R.id.btn_error_status);
        btn_error_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != loadingDialog && loadingDialog.isShowing()) {
                    return;
                }

                loadingDialog = new LoadingDialog(MainActivity.this);
                loadingDialog.show();

                byte[] command = {0x10, 0x04, 0x03};

                getPrinter().sendSingleData(command, getCallback2((TextView) findViewById(R.id.tv_error_status)));
            }
        });

        // 传送纸状态
        View btn_paper_status = findViewById(R.id.btn_paper_status);
        btn_paper_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != loadingDialog && loadingDialog.isShowing()) {
                    return;
                }

                loadingDialog = new LoadingDialog(MainActivity.this);
                loadingDialog.show();

                byte[] command = {0x10, 0x04, 0x04};

                getPrinter().sendSingleData(command, getCallback2((TextView) findViewById(R.id.tv_paper_status)));
            }
        });

//        testUSB();
        testUSB2();

    }

    @Override
    protected void onDestroy() {

        unregisterForContextMenu(findViewById(R.id.btn_print_image));
        unregisterForContextMenu(findViewById(R.id.btn_process_image));

        super.onDestroy();
    }

    private MenuItem menu_Cancel;
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        for (int i = 0; i < mipmapTitles.length; i ++) {
            menu.add(Menu.NONE, Menu.FIRST+i, Menu.NONE, mipmapTitles[i]);
        }
        menu_Cancel = menu.add(Menu.NONE, Menu.FIRST+mipmapTitles.length, Menu.NONE, "取消");

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (!item.equals(menu_Cancel)) {
            int index = item.getItemId()-Menu.FIRST;
            int resID = mipmapIDs[index];

            if (printImage) {
                printBitmap(resID);
            }
            else {
                showBitmap(resID);
            }

        }
        menu_Cancel = null;

        return super.onContextItemSelected(item);
    }

    /**
     * 顯示原圖和二值化後的圖到ImageView上
     */
    private void showBitmap(int resID) {
        ImageView iv_src = (ImageView) findViewById(R.id.iv_src);
        iv_src.setImageResource(resID);

        ImageView iv_bw = (ImageView) findViewById(R.id.iv_bw);
        iv_bw.setImageBitmap(binaryBitmap2(resID));
//        iv_bw.setImageBitmap(histogramImage_y(resID));
    }

    /**
     * 列印圖片
     */
    private void printBitmap(int resID) {
        if (null != loadingDialog && loadingDialog.isShowing()) {
            return;
        }

        loadingDialog = new LoadingDialog(MainActivity.this);
        loadingDialog.show();

        showBitmap(resID);

        Bitmap bitmap = loadBitmap(resID, 380);

        getPrinter().sendBitmap(bitmap, getCallback());
    }

    /**
     * 印假發票+假明細
     */
    private void printInvoiceAndDetail() {
        if (null != loadingDialog && loadingDialog.isShowing()) {
            return;
        }

        loadingDialog = new LoadingDialog(MainActivity.this);
        loadingDialog.show();

        final Handler handler = new Handler(Looper.getMainLooper());
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap b1 = createSampleInvoiceImage();
                byte[] cmd1;
                if (cb_use_embedded_printer.isChecked()) {
                    cmd1 = DPEmbeddedPrinterCommand.printImage(b1);
                }
                else {
                    cmd1 = GprinterCommand.printImage(b1);
                }
                b1.recycle();

                Bitmap b2 = createFakeLongDetailImage();
                byte[] cmd2;
                if (cb_use_embedded_printer.isChecked()) {
                    cmd2 = DPEmbeddedPrinterCommand.printImage(b2);
                }
                else {
                    cmd2 = GprinterCommand.printImage(b2);
                }
                b2.recycle();

                byte[] cmd;
                if (cb_use_embedded_printer.isChecked()) {
                    cmd = new DPEmbeddedPrinterCommand.Builder()
                                                    .append(cmd1) // 印發票指令
                                                    .append(DPEmbeddedPrinterCommand.cutPaper()) //切紙
                                                    .append(DPEmbeddedPrinterCommand.printAndFeedLines(1)) // 空一行
                                                    .append(cmd2) // 印明細指令
                                                    .append(DPEmbeddedPrinterCommand.printAndFeedLines(4)) // 空4行
                                                    .append(DPEmbeddedPrinterCommand.cutPaper()) // 切紙
                                                    .create();
                }
                else {
                    cmd = new GprinterCommand.Builder()
                                                .append(cmd1) // 印發票指令
                                                .append(GprinterCommand.cutPaper()) //切紙
                                                .append(GprinterCommand.printAndFeedLines(1)) // 空一行
                                                .append(cmd2) // 印明細指令
                                                .append(GprinterCommand.printAndFeedLines(4)) // 空4行
                                                .append(GprinterCommand.cutPaper()) // 切紙
                                                .create();
                }

                final byte[] finalCmd = cmd;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        getPrinter().sendData(finalCmd, getCallback());
                    }
                });
            }
        });
        thread.start();

    }

    /**
     * 從resource讀入Bitmap
     *
     * @param resource resource id
     * @param maxWidth 回傳的Bitmap的寬度
     */
    private Bitmap loadBitmap(int resource, int maxWidth) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap b = BitmapFactory.decodeResource(getResources(), resource, options);

        int width = b.getWidth();
        int height = b.getHeight();
        if (maxWidth > 0) {
            float scale = maxWidth/(float)width;
            width *= scale;
            height *= scale;
        }

        Bitmap b2 = Bitmap.createBitmap(width, height, b.getConfig());
        Canvas c = new Canvas(b2);
        c.drawBitmap(b, null, new Rect(0, 0, width, height), null);
//        c.drawBitmap(b, 0, 0, null);
        b.recycle();

        return b2;
    }

    /**
     * 圖片二值化 (助銓在iOS的Gprinter SDK的算法)
     */
    private Bitmap binaryBitmap1(int resource) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), resource, options);

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        IntBuffer pixelBuffer = IntBuffer.allocate(width*height);
        bitmap.getPixels(pixelBuffer.array(), 0, width, 0, 0, width, height);

        for (int h = 0; h < height; h ++) {
            for (int w = 0; w < width; w ++) {
                int index = h * width + w;
                int argb = pixelBuffer.get(index);
                if (pixelBrightness(argb) <= 127) {
                    pixelBuffer.put(index, Color.BLACK);
                }
                else {
                    pixelBuffer.put(index, Color.WHITE);
                }
            }
        }

        Bitmap b2 = Bitmap.createBitmap(width, height, bitmap.getConfig());
        b2.setPixels(pixelBuffer.array(), 0, width, 0, 0, width, height);
        bitmap.recycle();
        pixelBuffer.clear();

        return b2;
    }

    /**
     * 圖片二值化<br>
     * http://stackoverflow.com/questions/9377786/android-converting-a-bitmap-to-a-monochrome-bitmap-1-bit-per-pixel
     */
    private Bitmap binaryBitmap2(int resource) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), resource, options);

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        IntBuffer pixelBuffer = IntBuffer.allocate(width*height);
        bitmap.getPixels(pixelBuffer.array(), 0, width, 0, 0, width, height);

        float threshold = otsu_v(pixelBuffer);
//        threshold *= 1.05;
        Log.e(tag, "otsu threshold = " + threshold);

        float[] hsv = new float[3];
        for (int h = 0; h < height; h ++) {
            for (int w = 0; w < width; w ++) {
                int index = h * width + w;
                int argb = pixelBuffer.get(index);

                Color.colorToHSV(argb, hsv);
//                if (hsv[2] > 0.5f) {
                if (hsv[2] > threshold) {
                    pixelBuffer.put(index, Color.WHITE);
                }
                else {
                    pixelBuffer.put(index, Color.BLACK);
                }

//                int y = pixelBrightness(argb);
//                pixelBuffer.put(index, Color.argb(255, y, y, y));
            }
        }

        Bitmap b2 = Bitmap.createBitmap(width, height, bitmap.getConfig());
        b2.setPixels(pixelBuffer.array(), 0, width, 0, 0, width, height);
        bitmap.recycle();
        pixelBuffer.clear();

        return b2;
    }

    /**
     * 灰度直方圖
     */
    private Bitmap histogramImage_y(int resource) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), resource, options);

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        IntBuffer pixelBuffer = IntBuffer.allocate(width*height);
        bitmap.getPixels(pixelBuffer.array(), 0, width, 0, 0, width, height);
        bitmap.recycle();

        int scale = 255;
        int[] histogram = new int[scale+1];
        for (int i = 0; i < histogram.length; i ++) {
            histogram[i] = 0;
        }

//        float[] hsv = new float[3];
        int max = 0;
        for (int i = 0; i < pixelBuffer.capacity(); i ++) {
            int argb = pixelBuffer.get(i);

            int intVal = pixelBrightness(argb);
            histogram[intVal] ++;
//            Color.colorToHSV(argb, hsv);
//            int intVal = (int) (hsv[2]*scale);
//            histogram[intVal] ++;

            if (histogram[intVal] > max) {
                max = histogram[intVal];
            }
        }
        pixelBuffer.clear();

        Bitmap b = Bitmap.createBitmap((scale+1)*7, 500, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(b);
        c.drawARGB(255, 255, 255, 255);

        Paint p = new Paint();
        p.setColor(Color.BLACK);
        p.setStrokeWidth(2);
        c.drawLine(1, b.getHeight()-2, b.getWidth()-1, b.getHeight()-2, p);
        c.drawLine(1, b.getHeight()-2, 1, b.getHeight()-2, p);

        for (int i = 0; i < histogram.length; i ++) {
            int value = 490*histogram[i]/max;
            int xVal = i*7;

            c.drawLine(xVal, b.getHeight()-value-1, xVal, b.getHeight()-1, p);
        }

        return b;
    }

    /**
     * OTSU 演算法 (使用HSV的V值計算二值化的閥值)
     */
    private float otsu_v(IntBuffer pixelBuffer) {
        int scale = 255;
        int[] histogram = new int[scale+1];
        for (int i = 0; i < histogram.length; i ++) {
            histogram[i] = 0;
        }

        float[] hsv = new float[3];
        for (int i = 0; i < pixelBuffer.capacity(); i ++) {
            int argb = pixelBuffer.get(i);

            Color.colorToHSV(argb, hsv);
            int intVal = (int) (hsv[2]*scale);
            histogram[intVal] ++;
        }

        int sum = 0;
        int total = pixelBuffer.capacity();
        for (int i = 0; i < histogram.length; i ++) {
            sum += (i * histogram[i]);
        }

        int sumB = 0;
        float wB = 0;
        float wF;
        float mB;
        float mF;
        float max = 0.0f;
        float between;
        int threshold1 = 0;
        int threshold2 = 0;
        for (int i = 0; i < histogram.length; i ++) {
            wB += histogram[i];
            if (wB == 0) {
                continue;
            }
            wF = total - wB;
            if (wF == 0) {
                break;
            }
            sumB += i * histogram[i];
            mB = sumB / wB;
            mF = (sum - sumB) / wF;
            between = wB * wF * (mB - mF) * (mB - mF);
            if ( between >= max ) {
                threshold1 = i;
                if ( between > max ) {
                    threshold2 = i;
                }
                max = between;
            }

        }

        return (threshold1+threshold2) / (scale*2.0f);
    }

    /**
     * OTSU 演算法 (使用YUV的Y值計算二值化的閥值)
     */
    private int otsu_y(IntBuffer pixelBuffer) {
        int scale = 255;
        int[] histogram = new int[scale+1];
        for (int i = 0; i < histogram.length; i ++) {
            histogram[i] = 0;
        }

        for (int i = 0; i < pixelBuffer.capacity(); i ++) {
            int argb = pixelBuffer.get(i);

            int intVal = pixelBrightness(argb);
            histogram[intVal] ++;
        }

        int sum = 0;
        int total = pixelBuffer.capacity();
        for (int i = 0; i < histogram.length; i ++) {
            sum += (i * histogram[i]);
        }

        int sumB = 0;
        float wB = 0;
        float wF;
        float mB;
        float mF;
        float max = 0.0f;
        float between;
        int threshold1 = 0;
        int threshold2 = 0;
        for (int i = 0; i < histogram.length; i ++) {
            wB += histogram[i];
            if (wB == 0) {
                continue;
            }
            wF = total - wB;
            if (wF == 0) {
                break;
            }
            sumB += i * histogram[i];
            mB = sumB / wB;
            mF = (sum - sumB) / wF;
            between = wB * wF * (mB - mF) * (mB - mF);
            if ( between >= max ) {
                threshold1 = i;
                if ( between > max ) {
                    threshold2 = i;
                }
                max = between;
            }

        }

        return (threshold1+threshold2) / 2;
    }

    /**
     * 取得像素的亮度值 (YUV的Y)
     */
    private static int pixelBrightness(int argb) {
        int r = Color.red(argb);
        int g = Color.green(argb);
        int b = Color.blue(argb);

        return ((66*r + 129*g + 25*b + 128) >> 8) + 16; //y
//        return (r + g + b) /3;
    }

    /** 取得連接中的第一個USB裝置 */
    private UsbDevice getFirstConnectedDevice() {
        List<UsbDevice> devices;
        if (cb_use_embedded_printer.isChecked()) {
            devices = DPEmbeddedPrinterUsbManager.getUsbDeviceList(this);
        }
        else {
            devices = GprinterUsbManager.getUsbDeviceList(this);
        }

        if (null == devices || devices.size() == 0) {
            return null;
        }

        return devices.get(0);
    }

    /**
     * 在log印出連接到的USB裝置資訊 (測試用)
     */
    private void testUSB() {
//        List<UsbDevice> devices = GprinterUsbManager.getUsbDeviceList(this);
        List<UsbDevice> devices = DPEmbeddedPrinterUsbManager.getUsbDeviceList(this);
        if (null == devices || devices.size() == 0) {
            return;
        }

        UsbDevice device = devices.get(0);

        String debugString = "";
        debugString += "DeviceName = " +device.getDeviceName() +"\n";
        debugString += "DeviceId = " +device.getDeviceId() +"\n";
        debugString += "DeviceClass = " +device.getDeviceClass() +"\n";
        debugString += "DeviceSubclass = " +device.getDeviceSubclass() +"\n";
        debugString += "DeviceProtocol = " +device.getDeviceProtocol() +"\n";
        debugString += "pid = " +device.getProductId() +"\n";
        debugString += "vid = " +device.getVendorId() +"\n";
        Log.e(tag, "DeviceName = " +device.getDeviceName());
        Log.e(tag, "DeviceId = " +device.getDeviceId());
        Log.e(tag, "DeviceClass = " +device.getDeviceClass());
        Log.e(tag, "DeviceSubclass = " +device.getDeviceSubclass());
        Log.e(tag, "DeviceProtocol = " +device.getDeviceProtocol());
        Log.e(tag, "pid = " +device.getProductId());
        Log.e(tag, "vid = " +device.getVendorId());

        int iCnt = device.getInterfaceCount();
        debugString += "InterfaceCount = " +iCnt +"\n";
        Log.e(tag, "InterfaceCount = " +iCnt);
        for (int i = 0; i < iCnt; i ++) {
            UsbInterface usbInterface = device.getInterface(i);
            debugString += "    InterfaceID = " +usbInterface.getId() +"\n";
            debugString += "    InterfaceClass = "+usbInterface.getInterfaceClass() +"\n";
            debugString += "    InterfaceSubclass = "+usbInterface.getInterfaceSubclass() +"\n";
            debugString += "    InterfaceProtocol = "+usbInterface.getInterfaceProtocol() +"\n";
            Log.e(tag, "    InterfaceID = " +usbInterface.getId());
            Log.e(tag, "    InterfaceClass = "+usbInterface.getInterfaceClass());
            Log.e(tag, "    InterfaceSubclass = "+usbInterface.getInterfaceSubclass());
            Log.e(tag, "    InterfaceProtocol = "+usbInterface.getInterfaceProtocol());

            int endCnt = usbInterface.getEndpointCount();
            debugString += "    EndpointCount = " +endCnt +"\n";
            Log.e(tag, "    EndpointCount = " +endCnt);
            for (int j = 0; j < endCnt; j ++) {
                UsbEndpoint endpoint = usbInterface.getEndpoint(j);
                debugString += "        Address = "+endpoint.getAddress() +"\n";
                debugString += "        Attributes = "+endpoint.getAttributes() +"\n";
                debugString += "        Direction = "+endpoint.getDirection() +"\n";
                debugString += "        EndpointNumber = "+endpoint.getEndpointNumber() +"\n";
                debugString += "        Interval = "+endpoint.getInterval() +"\n";
                debugString += "        MaxPacketSize = "+endpoint.getMaxPacketSize() +"\n";
                debugString += "        Type = "+endpoint.getType() +"\n";
                Log.e(tag, "        Address = "+endpoint.getAddress());
                Log.e(tag, "        Attributes = "+endpoint.getAttributes());
                Log.e(tag, "        Direction = "+endpoint.getDirection());
                Log.e(tag, "        EndpointNumber = "+endpoint.getEndpointNumber());
                Log.e(tag, "        Interval = "+endpoint.getInterval());
                Log.e(tag, "        MaxPacketSize = "+endpoint.getMaxPacketSize());
                Log.e(tag, "        Type = "+endpoint.getType());
                if (j != endCnt-1) {
                    debugString += "        ------------------------------------" +"\n";
                    Log.e(tag, "        ------------------------------------");
                }
            }

            if (i != iCnt-1) {
                debugString += "    --------------------------------" +"\n";
                Log.e(tag, "    --------------------------------");
            }

        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            debugString += "-------------------------------------------------" +"\n";
            Log.e(tag, "-------------------------------------------------");
            int configCnt = device.getConfigurationCount();
            debugString += "ConfigurationCount = " +configCnt +"\n";
            Log.e(tag, "ConfigurationCount = " +configCnt);
            for (int k = 0; k < configCnt; k ++) {
                UsbConfiguration config = device.getConfiguration(k);
                debugString += "    ConfigurationName = " +config.getName() +"\n";
                debugString += "    ConfigurationId = "+config.getId() +"\n";
                debugString += "    MaxPower = "+config.getMaxPower() +"\n";
                Log.e(tag, "    ConfigurationName = " +config.getName());
                Log.e(tag, "    ConfigurationId = "+config.getId());
                Log.e(tag, "    MaxPower = "+config.getMaxPower());

                iCnt = config.getInterfaceCount();
                debugString += "    InterfaceCount = " +iCnt +"\n";
                Log.e(tag, "    InterfaceCount = " +iCnt);
                for (int i = 0; i < iCnt; i ++) {
                    UsbInterface usbInterface = config.getInterface(i);
                    debugString += "        InterfaceID = " +usbInterface.getId() +"\n";
                    debugString += "        InterfaceClass = "+usbInterface.getInterfaceClass() +"\n";
                    debugString += "        InterfaceSubclass = "+usbInterface.getInterfaceSubclass() +"\n";
                    debugString += "        InterfaceProtocol = "+usbInterface.getInterfaceProtocol() +"\n";
                    Log.e(tag, "        InterfaceID = " +usbInterface.getId());
                    Log.e(tag, "        InterfaceClass = "+usbInterface.getInterfaceClass());
                    Log.e(tag, "        InterfaceSubclass = "+usbInterface.getInterfaceSubclass());
                    Log.e(tag, "        InterfaceProtocol = "+usbInterface.getInterfaceProtocol());

                    int endCnt = usbInterface.getEndpointCount();
                    debugString += "    EndpointCount = " +endCnt +"\n";
                    Log.e(tag, "    EndpointCount = " +endCnt);
                    for (int j = 0; j < endCnt; j ++) {
                        UsbEndpoint endpoint = usbInterface.getEndpoint(j);
                        debugString += "            Address = "+endpoint.getAddress() +"\n";
                        debugString += "            Attributes = "+endpoint.getAttributes() +"\n";
                        debugString += "            Direction = "+endpoint.getDirection() +"\n";
                        debugString += "            EndpointNumber = "+endpoint.getEndpointNumber() +"\n";
                        debugString += "            Interval = "+endpoint.getInterval() +"\n";
                        debugString += "            MaxPacketSize = "+endpoint.getMaxPacketSize() +"\n";
                        debugString += "            Type = "+endpoint.getType() +"\n";
                        Log.e(tag, "            Address = "+endpoint.getAddress());
                        Log.e(tag, "            Attributes = "+endpoint.getAttributes());
                        Log.e(tag, "            Direction = "+endpoint.getDirection());
                        Log.e(tag, "            EndpointNumber = "+endpoint.getEndpointNumber());
                        Log.e(tag, "            Interval = "+endpoint.getInterval());
                        Log.e(tag, "            MaxPacketSize = "+endpoint.getMaxPacketSize());
                        Log.e(tag, "            Type = "+endpoint.getType());
                        if (j != endCnt-1) {
                            debugString += "            ------------------------------------" +"\n";
                            Log.e(tag, "            ------------------------------------");
                        }
                    }

                    if (i != iCnt-1) {
                        debugString += "        --------------------------------" +"\n";
                        Log.e(tag, "        --------------------------------");
                    }

                }

            }
        }

        tv_debug.append("\n"+debugString);

    }

    private void testUSB2() {
        UsbManager manager = (UsbManager) getApplicationContext().getSystemService(Context.USB_SERVICE);
        // Get the list of attached devices
        HashMap<String, UsbDevice> devices = manager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = devices.values().iterator();
        int count = devices.size();

        ArrayList<UsbDevice> gpDevices = new ArrayList<>();
        if (count > 0) {
            String debugString = "";
            int index = 0;
            while (deviceIterator.hasNext()) {
                UsbDevice device = deviceIterator.next();

                debugString += ("["+index+"]("+device.getDeviceName()+")"+"vid="+device.getVendorId()+",pid="+device.getProductId()+"\n");
                index ++;
            }
            tv_debug.append("\n"+debugString);
        }
        else {
            tv_debug.append("\nNo Usb Device\n");
        }

    }

    /**
     * 根據設定，回傳對應的Printer實體
     */
    private PrinterBase getPrinter() {
        if (cb_use_embedded_printer.isChecked()) {
            return new DPEmbeddedPrinterHelper(MainActivity.this, getFirstConnectedDevice());
        }

        GprinterHelper printer;
        if (cb_use_usb.isChecked()) {
            printer = new GprinterHelper(MainActivity.this, getFirstConnectedDevice());
        }
        else {
            String host = et_host.getText().toString();
            int port = Integer.parseInt(et_port.getText().toString());
            printer = new GprinterHelper(MainActivity.this, host, port);
        }

        return printer;
    }

    /**
     * 回傳產生共用的Callback
     */
    private PrinterBase.Callback getCallback() {
        return new PrinterBase.Callback() {
            @Override
            public void onComplete(boolean success, String printerStatus, String errorMessage) {
                if (loadingDialog != null){
                    loadingDialog.dismiss();
                }

                tv_debug.append("\n"+"Complete: "+success+", printerStatus="+printerStatus+"\n");
                Log.e(tag, "Complete: "+success);

                if (!success) {
                    showMessageAlertView(errorMessage);
                }

            }

            @Override
            public void onFailure(String printerStatus, String errorMessage) {
                if (loadingDialog != null){
                    loadingDialog.dismiss();
                }

                tv_debug.append("\n"+"Failure"+", printerStatus="+printerStatus+"\n");
                Log.e(tag, "Failure");

                showMessageAlertView(errorMessage);

            }
        };
    }

    /**
     * 回傳產生共用的Callback
     */
    private PrinterBase.Callback getCallback2(final TextView textView) {
        return new PrinterBase.Callback() {
            @Override
            public void onComplete(boolean success, String printerStatus, String errorMessage) {
                if (loadingDialog != null){
                    loadingDialog.dismiss();
                }

                tv_debug.append("\n"+"Complete: "+success+", printerStatus="+printerStatus+"\n");
                Log.e(tag, "Complete: "+success);

                textView.setText(printerStatus);

            }

            @Override
            public void onFailure(String printerStatus, String errorMessage) {
                if (loadingDialog != null){
                    loadingDialog.dismiss();
                }

                tv_debug.append("\n"+"Failure"+", printerStatus="+printerStatus+"\n");
                Log.e(tag, "Failure");

                textView.setText(printerStatus);

            }
        };
    }

    /**
     * 顯示Alert
     */
    private void showMessageAlertView(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
                                                .setTitle(message)
                                                .setNeutralButton("OK", null)
                                                .create();
        alertDialog.show();
    }

    /**
     * 產生假的電子發票圖片
     */
    private Bitmap createSampleInvoiceImage() {
        int maxWidth = 380;
        int maxHeight = 600;
        Bitmap bm = Bitmap.createBitmap(maxWidth, maxHeight, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bm);
        c.drawRGB(255, 255, 255);

        // 最上方的虛線，不需要了
//        PathEffect pathEffect = new DashPathEffect(new float[]{10, 5, 10, 5}, 0);
//        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        paint.setPathEffect(pathEffect);
//        paint.setColor(Color.BLACK);
//        paint.setStrokeWidth(4);
//        paint.setStyle(Paint.Style.STROKE);
//
//        Path path = new Path();
//        path.moveTo(0, 0);
//        path.lineTo(maxWidth, 0);
//        c.drawPath(path, paint);

        Paint tp = new Paint(Paint.ANTI_ALIAS_FLAG);
        tp.setTextAlign(Paint.Align.CENTER);
        tp.setColor(Color.BLACK);

        tp.setTextSize(38);
        float offY=0;
        Paint.FontMetrics fm = tp.getFontMetrics();
        float fontTotalHeight = fm.bottom - fm.top;
        float offY1 = fontTotalHeight / 2 - fm.bottom;
        float newY = offY + fontTotalHeight/2 + offY1;

        String txt = "健中資訊股份有限公司";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-10;
        tp.setTextSize(43);
        tp.setFakeBoldText(false);
        fm = tp.getFontMetrics();
        fontTotalHeight = fm.bottom - fm.top;
        offY1 = fontTotalHeight / 2 - fm.bottom;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "電子發票證明聯補印";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-10;
        tp.setTextSize(47);
        tp.setFakeBoldText(true);
        fm = tp.getFontMetrics();
        fontTotalHeight = fm.bottom - fm.top;
        offY1 = fontTotalHeight / 2 - fm.bottom;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "105年09-10月";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-10;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "KT-28773338";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-5;
        Log.e("TEST-Y-1", "offY="+offY); // 353.45117, 313.45117
        tp.setTextSize(24);
        tp.setFakeBoldText(false);
        tp.setTextAlign(Paint.Align.LEFT);
        fm = tp.getFontMetrics();
        fontTotalHeight = fm.bottom - fm.top;
        offY1 = fontTotalHeight / 2 - fm.bottom;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "2016/10/11 06:48:45";
        c.drawText(txt, 0, txt.length(), 20, newY, tp);

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "隨機碼: 2956";
        c.drawText(txt, 0, txt.length(), 20, newY, tp);

        txt = "總計: 990";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "賣方: 10718853";
        c.drawText(txt, 0, txt.length(), 20, newY, tp);

        txt = "買方: 53796668";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight;
        Log.e("TEST-Y-2", "offY="+offY); //495.88672, 425.88672

        /*
          Zxing內部計算不管怎樣都可能會在回傳的BitMatrix加上padding，所以這裡是仿Zxing的程式碼客制化產生Code39和QRCode的class
          (稍微修改一下程式碼，去掉產生padding的部分)
          參考: http://m.2cto.com/kf/201605/504382.html
         */

        offY = offY+20;
        try {
            int codeWidth = maxWidth;
            int codeHeight = 80;
            Hashtable<EncodeHintType, Object> hints = new Hashtable<>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
            hints.put(EncodeHintType.MARGIN, 0);
            BitMatrix bitMatrix = new MyCode39Writer().encode("10510LE422206938111", BarcodeFormat.CODE_39, codeWidth, codeHeight, hints);
            int[] pixels = new int[codeWidth*codeHeight];
            //下面這裡按照二維碼的算法，逐個生成二維碼的圖片，
            //兩個for循環是圖片橫列掃描的結果
            for (int y = 0; y < codeHeight; y++) {
                for (int x = 0; x < codeWidth; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * codeWidth + x] = Color.BLACK;
                    }
                    else {
                        pixels[y * codeWidth + x] = Color.WHITE;
                    }
                }
            }

            Bitmap d1code = Bitmap.createBitmap(pixels, codeWidth, codeHeight, Bitmap.Config.RGB_565);
            c.drawBitmap(d1code,
                    new Rect(0, 0, d1code.getWidth(), d1code.getHeight()),
                    new Rect(0, (int)offY, maxWidth, (int)(offY+80)),
                    new Paint(Paint.ANTI_ALIAS_FLAG));
            d1code.recycle();

        } catch (WriterException e) {
            e.printStackTrace();
        }
        offY = offY+80;
        Log.e("TEST-Y-3", "offY="+offY);

        int width = maxWidth/2;

        offY = offY+20;

//        int codeWidth = width/4;
        int codeWidth = 150;

        txt = "LE422206931050926811100000015000000160000000028426674yZ1zsLCd0XRVOg7VXOsI5A==:**********:5:5:1:24H光泉高鈣鮮豆漿:1:52:45 波的多蚵仔煎:1:16:";
        try {
            Hashtable<EncodeHintType, Object> hints = new Hashtable<>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
            hints.put(EncodeHintType.MARGIN, 0);
            hints.put(EncodeHintType.DATA_MATRIX_SHAPE, SymbolShapeHint.FORCE_SQUARE);
            BitMatrix bitMatrix = new MyQRCodeWriter().encode(txt, BarcodeFormat.QR_CODE, codeWidth, codeWidth, hints);
            int[] pixels = new int[codeWidth*codeWidth];
            //下面這裡按照二維碼的算法，逐個生成二維碼的圖片，
            //兩個for循環是圖片橫列掃描的結果
            for (int y = 0; y < codeWidth; y++) {
                for (int x = 0; x < codeWidth; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * codeWidth + x] = Color.BLACK;
                    }
                    else {
                        pixels[y * codeWidth + x] = Color.WHITE;
                    }
                }
            }

            Bitmap d2code = Bitmap.createBitmap(pixels, codeWidth, codeWidth, Bitmap.Config.RGB_565);
            c.drawBitmap(d2code,
                    new Rect(0, 0, d2code.getWidth(), d2code.getHeight()),
                    new Rect(10, (int) offY, width-10, (int) (offY+width-20)),
                    new Paint(Paint.ANTI_ALIAS_FLAG));
            d2code.recycle();

//            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
//            p.setColor(Color.BLACK);
//            p.setStrokeWidth(1);
//            p.setStyle(Paint.Style.STROKE);
//            c.drawRect(10, (int) offY, width-10, (int) (offY+width-20), p);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        txt = "**45 波的多蚵仔煎:1:16:90 統一肉燥麵碗:1:19:68 統一肉骨茶碗:1:19";
//        txt = "**";
        try {
            Hashtable<EncodeHintType, Object> hints = new Hashtable<>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
            hints.put(EncodeHintType.MARGIN, 0);
            hints.put(EncodeHintType.DATA_MATRIX_SHAPE, SymbolShapeHint.FORCE_SQUARE);
            BitMatrix bitMatrix = new MyQRCodeWriter().encode(txt, BarcodeFormat.QR_CODE, codeWidth, codeWidth, hints);
            int[] pixels = new int[codeWidth*codeWidth];
            //下面這裡按照二維碼的算法，逐個生成二維碼的圖片，
            //兩個for循環是圖片橫列掃描的結果
            for (int y = 0; y < codeWidth; y++) {
                for (int x = 0; x < codeWidth; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * codeWidth + x] = Color.BLACK;
                    }
                    else {
                        pixels[y * codeWidth + x] = Color.WHITE;
                    }
                }
            }

            Bitmap d2code = Bitmap.createBitmap(pixels, codeWidth, codeWidth, Bitmap.Config.RGB_565);
            c.drawBitmap(d2code,
                    new Rect(0, 0, d2code.getWidth(), d2code.getHeight()),
                    new Rect(width+10, (int) offY, maxWidth-10, (int) (offY+width-20)),
                    new Paint(Paint.ANTI_ALIAS_FLAG));
            d2code.recycle();

//            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
//            p.setColor(Color.BLACK);
//            p.setStrokeWidth(1);
//            p.setStyle(Paint.Style.STROKE);
//            c.drawRect(width+10, (int) offY, maxWidth-10, (int) (offY+width-20), p);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        offY = offY+width-20;
        Log.e("TEST-Y-4", "offY="+offY);

        /* 畫虛線，有切紙功能的Gprinter不需要
        PathEffect pathEffect1 = new DashPathEffect(new float[]{10, 5, 10, 5}, 0);
        Paint paint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint1.setPathEffect(pathEffect1);
        paint1.setColor(Color.BLACK);
        paint1.setStrokeWidth(4);
        paint1.setStyle(Paint.Style.STROKE);

        Path path1 = new Path();
        path1.moveTo(0, c.getHeight()-5);
        path1.lineTo(maxWidth, c.getHeight()-5);
        c.drawPath(path1, paint1);
        */

        return bm;
    }

    /**
     * 產生測試用的銷貨明細圖片<br>
     * 這個函式是測試讓Gprinter印很長的圖片用的，所以裡頭的格式是隨便寫的
     */
    private Bitmap createFakeLongDetailImage() {
        int maxWidth = 380;
        int maxHeight = 1000;
        Bitmap bm = Bitmap.createBitmap(maxWidth, maxHeight, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bm);
        c.drawRGB(255, 255, 255);

        Paint tp = new Paint(Paint.ANTI_ALIAS_FLAG);
        tp.setTextAlign(Paint.Align.CENTER);
        tp.setColor(Color.BLACK);

        tp.setTextSize(38);
        float offY=0;
        Paint.FontMetrics fm = tp.getFontMetrics();
        float fontTotalHeight = fm.bottom - fm.top;
        float offY1 = fontTotalHeight / 2 - fm.bottom;
        float newY = offY + fontTotalHeight/2 + offY1;

        String txt = "銷貨明細";
        c.drawText(txt, 0, txt.length(), maxWidth/2, newY, tp);

        offY = offY+fontTotalHeight+20;
        tp.setTextSize(22);
        tp.setFakeBoldText(false);
        tp.setTextAlign(Paint.Align.LEFT);
        fm = tp.getFontMetrics();
        fontTotalHeight = fm.bottom - fm.top;
        offY1 = fontTotalHeight / 2 - fm.bottom;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "營業人名稱: 健中資訊";
        c.drawText(txt, 0, txt.length(), 0, newY, tp);

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "營業人統編: 53796668";
        c.drawText(txt, 0, txt.length(), 0, newY, tp);

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "電話: 0952-314-802";
        c.drawText(txt, 0, txt.length(), 0, newY, tp);

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "交易時間: 2016/10/11 06:48:45";
        c.drawText(txt, 0, txt.length(), 0, newY, tp);

        for (int i = 0; i < 20; i ++) {
            offY = offY+fontTotalHeight-5;
            newY = offY + fontTotalHeight/2 + offY1;
            txt = "品項"+(i+1)+"  x1  NTD. "+(i+1)+"00";
            c.drawText(txt, 0, txt.length(), 0, newY, tp);
        }

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "商品小計  共20項  NTD. 999999";
        c.drawText(txt, 0, txt.length(), 0, newY, tp);

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "總計  NTD. 999999";
        c.drawText(txt, 0, txt.length(), 0, newY, tp);

        offY = offY+fontTotalHeight-5;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "稅額  NTD. 0";
        c.drawText(txt, 0, txt.length(), 0, newY, tp);

        offY = offY+fontTotalHeight-5;
        tp.setTextSize(30);
        tp.setFakeBoldText(true);
        fm = tp.getFontMetrics();
        fontTotalHeight = fm.bottom - fm.top;
        offY1 = fontTotalHeight / 2 - fm.bottom;
        newY = offY + fontTotalHeight/2 + offY1;
        txt = "支付金額  NTD. 99999999";
        c.drawText(txt, 0, txt.length(), 0, newY, tp);

        return bm;
    }

}
