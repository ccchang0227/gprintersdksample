package com.realtouchapp.barcodeencoder;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.OneDimensionalCodeWriter;

import java.util.Map;

/**
 * Created by C.C.Chang on 2017/5/15.
 *
 * @version 1
 */

public final class MyCode39Writer extends OneDimensionalCodeWriter {

    private static final String ALPHABET_STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. *$/+%";

    private static final int[] CHARACTER_ENCODINGS = {
            0x034, 0x121, 0x061, 0x160, 0x031, 0x130, 0x070, 0x025, 0x124, 0x064, // 0-9
            0x109, 0x049, 0x148, 0x019, 0x118, 0x058, 0x00D, 0x10C, 0x04C, 0x01C, // A-J
            0x103, 0x043, 0x142, 0x013, 0x112, 0x052, 0x007, 0x106, 0x046, 0x016, // K-T
            0x181, 0x0C1, 0x1C0, 0x091, 0x190, 0x0D0, 0x085, 0x184, 0x0C4, 0x094, // U-*
            0x0A8, 0x0A2, 0x08A, 0x02A // $-%
    };

    @Override
    public BitMatrix encode(String contents,
                            BarcodeFormat format,
                            int width,
                            int height,
                            Map<EncodeHintType,?> hints) throws WriterException {
        if (format != BarcodeFormat.CODE_39) {
            throw new IllegalArgumentException("Can only encode CODE_39, but got " + format);
        }
        return super.encode(contents, format, width, height, hints);
    }

    @Override
    public boolean[] encode(String contents) {
        int length = contents.length();
        if (length > 80) {
            throw new IllegalArgumentException(
                    "Requested contents should be less than 80 digits long, but got " + length);
        }

        int[] widths = new int[9];
        int codeWidth = 24 + 1 + length;
        for (int i = 0; i < length; i++) {
            int indexInString = MyCode39Writer.ALPHABET_STRING.indexOf(contents.charAt(i));
            if (indexInString < 0) {
                throw new IllegalArgumentException("Bad contents: " + contents);
            }
            toIntArray(MyCode39Writer.CHARACTER_ENCODINGS[indexInString], widths);
            for (int width : widths) {
                codeWidth += width;
            }
        }
        boolean[] result = new boolean[codeWidth];
        toIntArray(MyCode39Writer.CHARACTER_ENCODINGS[39], widths);
        int pos = appendPattern(result, 0, widths, true);
        int[] narrowWhite = {1};
        pos += appendPattern(result, pos, narrowWhite, false);
        //append next character to byte matrix
        for (int i = 0; i < length; i++) {
            int indexInString = MyCode39Writer.ALPHABET_STRING.indexOf(contents.charAt(i));
            toIntArray(MyCode39Writer.CHARACTER_ENCODINGS[indexInString], widths);
            pos += appendPattern(result, pos, widths, true);
            pos += appendPattern(result, pos, narrowWhite, false);
        }
        toIntArray(MyCode39Writer.CHARACTER_ENCODINGS[39], widths);
        appendPattern(result, pos, widths, true);
        return result;
    }

    private static void toIntArray(int a, int[] toReturn) {
        for (int i = 0; i < 9; i++) {
            int temp = a & (1 << (8 - i));
            toReturn[i] = temp == 0 ? 1 : 2;
        }
    }

    /**
     * @return a byte array of horizontal pixels (0 = white, 1 = black)
     */
    private static BitMatrix renderResult(boolean[] code, int width, int height, int sidesMargin) {
        int inputWidth = code.length;
        // Add quiet zone on both sides.
        int fullWidth = inputWidth + sidesMargin;
        int outputWidth = Math.max(width, fullWidth);
        int outputHeight = Math.max(1, height);

        int multiple = outputWidth / fullWidth;
        int leftPadding = 0;

        BitMatrix output = new BitMatrix(outputWidth, outputHeight);
        for (int inputX = 0, outputX = leftPadding; inputX < inputWidth; inputX++, outputX += multiple) {
            if (code[inputX]) {
                output.setRegion(outputX, 0, multiple, outputHeight);
            }
        }
        return output;
    }

}
